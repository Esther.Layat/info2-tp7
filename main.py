import csv
import math
import re

from sklearn.metrics import classification_report

categories = ("neg", "pos")


def extract_rows(filename):
    documents = []
    with open(filename, newline='', mode='r', encoding='UTF8') as f:
        reader = csv.reader(f)
        next(reader, None)
        for row in reader:
            documents.append((row[0], row[1]))
    return documents


def tokenize(text):
    return [t.lower() for t in re.findall(r'\w+|[^\w\s]+', text) if t.isalpha()]


def train(filename):
    m = {
        'document_count_neg': 0,
        'document_count_pos': 0,
        'word_count_neg': 0,
        'word_count_pos': 0,
        'vocabulary_neg': {},
        'vocabulary_pos': {},
        'vocabulary': {},
    }

    # TODO ouvrir et parcourir l'ensemble d'entraînement - utiliser extract_rows et tokenize
    # TODO enregistrer toutes les fréquences nécessaires à l’application du théorème de Bayes

    commentaires = extract_rows(filename)
    for commentaire in commentaires:
        tokens = tokenize(commentaire[1])
        if commentaire[0] == "pos":
            m["document_count_pos"] += 1
            for token in tokens:
                m["word_count_pos"] += 1
                if token in m['vocabulary_pos']:
                    m['vocabulary_pos'][token] += 1
                else:
                    m['vocabulary_pos'][token] = 1
                if token in m['vocabulary']:
                    m['vocabulary'][token] += 1
                else:
                    m['vocabulary'][token] = 1
        else:
            m["document_count_neg"] += 1
            for token in tokens:
                m["word_count_neg"] += 1
                if token in m['vocabulary_neg']:
                    m['vocabulary_neg'][token] += 1
                else:
                    m['vocabulary_neg'][token] = 1
                if token in m['vocabulary']:
                    m['vocabulary'][token] += 1
                else:
                    m['vocabulary'][token] = 1

    return m


def test(filename, model):
    p = {
        'category_expected': [],
        'category_predicted': [],
        'neg_correct': 0,
        'neg_incorrect': 0,
        'pos_correct': 0,
        'pos_incorrect': 0,
    }

    vocabulary_size = len(model['vocabulary'])
    probability_prior_neg = math.log(
        model['document_count_neg'] / (model['document_count_neg'] + model['document_count_pos']))
    probability_prior_pos = math.log(
        model['document_count_pos'] / (model['document_count_neg'] + model['document_count_pos']))

    # TODO ouvrir et parcourir l'ensemble de test - utiliser extract_rows et tokenize
    # TODO appliquer le théorème de Bayes pour prédire la classe de chaque critique

    commentaires = extract_rows(filename)
    for commentaire in commentaires:
        p['category_expected'].append(commentaire[0])
        tokens = tokenize(commentaire[1])

        bayes_pos = probability_prior_pos
        bayes_neg = probability_prior_neg

        for token in tokens:
            frequence_neg = 0
            if token in model["vocabulary_neg"]:
                frequence_neg = model["vocabulary_neg"][token]

            probability_prior_f_sachant_neg = math.log(
                (frequence_neg + 1) / (model["word_count_neg"] + vocabulary_size))
            bayes_neg += probability_prior_f_sachant_neg

            frequence_pos = 0
            if token in model["vocabulary_pos"]:
                frequence_pos = model["vocabulary_pos"][token]

            probability_prior_f_sachant_pos = math.log(
                (frequence_pos + 1) / (model["word_count_pos"] + vocabulary_size))
            bayes_pos += probability_prior_f_sachant_pos

        if bayes_pos > bayes_neg:
            p["category_predicted"].append("pos")
            if commentaire[0] == "pos":
                p["pos_correct"] += 1
            else:
                p["neg_incorrect"] += 1
        else:
            p["category_predicted"].append("neg")
            if commentaire[0] == "neg":
                p["neg_correct"] += 1
            else:
                p["pos_incorrect"] += 1
    return p


def evaluate(predictions):
    results = classification_report(predictions['category_expected'], predictions['category_predicted'],
                                    labels=("pos", "neg"), target_names=["Pos", "Neg"])

    print('\n\n###################### MATRICE DE CONFUSION #######################')
    print('\n\n', results, '\n\n')
    print('####################################################################')

    print('Critiques négatives devinées :', predictions['neg_correct'], end=' ')
    print('/', end=' ')
    print(predictions['neg_correct'] + predictions['neg_incorrect'])

    print('Critiques positives devinées :', predictions['pos_correct'], end=' ')
    print('/', end=' ')
    print(predictions['pos_correct'] + predictions['pos_incorrect'])

    correct = predictions['neg_correct'] + predictions['pos_correct']
    incorrect = predictions['neg_incorrect'] + predictions['pos_incorrect']

    accuracy = correct / (correct + incorrect)
    print('Exactitude :', accuracy * 100, "%")


if __name__ == '__main__':
    model = train('movie_review_train.csv')
    predictions = test('movie_review_test.csv', model)
    evaluate(predictions)
